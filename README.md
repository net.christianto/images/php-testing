# PHP Testing Image

This is `chez14`'s default docker image for testing PHP projects.

Supported PHP Versions:
- 8.1
- 8.2

This image are equiped up with:
- Composer
- PHPStan
- PHPUnit


## Usage:
In `.gitlab-ci.yml` you can use the image like this:

```yml
PHP UnitTest:
    image: registry.gitlab.com/net.christianto/images/php-testing:8.1
    stage: test
    coverage: /^\s*Lines:\s*\d+.\d+\%/
    variables:
        XDEBUG_MODE: coverage
    before_script:
        - composer config --global gitlab-oauth.gitlab.com "${CI_JOB_TOKEN}"
        - composer install --no-interaction --no-progress
    script:
        - composer run test -- --colors=never --log-junit report.xml --coverage-text --coverage-cobertura=coverage.cobertura.xml
    artifacts:
        reports:
            coverage_report:
                coverage_format: cobertura
                path: coverage.cobertura.xml
            junit: report.xml
```

## License

[MIT](./LICENSE).
