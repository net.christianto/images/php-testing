ARG PHP_BASE_VER=8.1
FROM php:${PHP_BASE_VER}-alpine

ENV DEV_DEPS=" \
    # Utils
    curl-dev pcre-dev \
    # Internationalization
    icu-dev \
    # Compressions
    libzip-dev bzip2-dev \
    # Databases
    libpq-dev unixodbc-dev sqlite-dev \
    # Parsers
    libxml2-dev expat-dev"

# GD Dependencies
ENV GD_DEPS="\
    # Fonts.
    freetype-dev \
    # JPEGs
    jpeg-dev libjpeg-turbo-dev \
    # PNGs
    zlib-dev libpng-dev \
    # Google's Image algo thingy.
    libwebp-dev libvpx-dev \
    # Forgot what this whats for /shrug.
    libxpm-dev"

RUN apk update -q && \
    apk --no-cache add --virtual .build-dep ${PHPIZE_DEPS} linux-headers && \
    apk --no-cache add git unzip \
    ${DEV_DEPS} ${GD_DEPS} && \
    ############################
    # Install Xdebug for code coverage!
    pecl install xdebug && \
    docker-php-ext-enable xdebug && \
    ####################
    # Configure GD Plugins
    docker-php-ext-configure gd \
    --with-webp=/usr/include/ \
    --with-jpeg=/usr/include/ \
    --with-xpm=/usr/include/ \
    --with-freetype=/usr/include/ && \
    ############################
    # Execute the thing!
    docker-php-ext-install pdo pdo_pgsql pdo_mysql intl xml zip bz2 gd && \
    docker-php-ext-enable gd && \
    ############################
    # AST Support
    pecl install ast && docker-php-ext-enable ast && \
    ############################
    # Cleanups!
    apk del .build-dep

################################################################################
## composer install
################################################################################
COPY --from=composer:2 /usr/bin/composer /usr/local/bin/composer

# Set composer home for installing things and run it on the wild.
ARG COMPOSER_HOME=/composer/
ENV COMPOSER_HOME=${COMPOSER_HOME}
ENV PATH=${PATH}:${COMPOSER_HOME}/vendor/bin/
